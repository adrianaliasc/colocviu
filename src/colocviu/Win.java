package colocviu;

import javax.swing.*;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/*
 Implement a Java GUI application composed of two text fields and one button.
 The first text field holds the name of a text file. When clicking the button the number
 of characters from the file is written in the second text field.
 */

public class Win extends JFrame {

    JButton button1;
    JTextField field1;
    JTextField field2;
    int chars;

    public Win() {
        super("Colocviu");
        init();
        this.setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void init() {
        this.setLayout(null);
        this.setBounds(500, 100, 220, 300);

        button1 = new JButton("Button 1");
        button1.setBounds(50, 100, 100, 25);
        this.add(button1);

        button1.addActionListener(e -> {
            chars = countCharacetrs(field1.getText());
            System.out.println(chars);
            field2.setText(chars + "");
        });
        System.out.println(chars);
        field1 = new JTextField();
        field1.setBounds(50, 50, 100, 25);
        this.add(field1);

        field2 = new JTextField();
        field2.setBounds(50, 150, 100, 25);
        this.add(field2);
    }

    int countCharacetrs(String fileFrom) {
        int count = 0;
        FileReader in = null;

        try {
            in = new FileReader("src/" + fileFrom);

            while(in.read() != -1) {
                count++;
            }


        } catch (IOException except){
            except.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ignored) {
            }
        }
        return count;
    }
}
